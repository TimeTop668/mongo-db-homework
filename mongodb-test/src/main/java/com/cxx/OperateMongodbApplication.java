package com.cxx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OperateMongodbApplication {
    public static void main(String[] args) {
        SpringApplication.run(OperateMongodbApplication.class, args);
    }
}
