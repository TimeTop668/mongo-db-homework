package com.cxx.Repository;

import com.cxx.pojo.LgResumeDatas;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface LgResumeDatasRepository extends MongoRepository<LgResumeDatas,Long> {
}
