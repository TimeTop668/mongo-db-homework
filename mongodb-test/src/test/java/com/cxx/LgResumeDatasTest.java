package com.cxx;

import com.cxx.pojo.LgResumeDatas;
import com.cxx.Repository.LgResumeDatasRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@SpringBootTest(classes = OperateMongodbApplication.class)
@RunWith(SpringRunner.class)
public class LgResumeDatasTest {

    @Autowired
    LgResumeDatasRepository lgResumeDatasRepository;

    @Test
    public void add(){
        for (int i = 0; i < 100; i++) {
            LgResumeDatas lgResumeDatas = new LgResumeDatas();
            lgResumeDatas.setName("李"+i);
            lgResumeDatas.setAge((int)(Math.random()*100)+1);
            lgResumeDatasRepository.insert(lgResumeDatas);
        }
    }

    @Test
    public void findAll(){
        List<LgResumeDatas> all = lgResumeDatasRepository.findAll();
        for (int i = 0; i < all.size(); i++) {
            System.out.println(all.get(i));
        }
    }

    @Test
    public void delete(){
        lgResumeDatasRepository.deleteAll();
    }
}
